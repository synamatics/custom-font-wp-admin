<?php
/**
 * Plugin Name
 *
 * @package           cf-wp-admin
 * @author            Vishnu Raj
 * @copyright         2020 Synamatics
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Custom Font Wordpress Admin
 * Plugin URI:        https://synamatics.gitlab.io/custom-font-wp-admin/
 * Description:       Malayalam Fontify on Wordpress
 * Version:           1.0.1
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Vishnu Raj
 * Author URI:        https://synamatics.gitlab.io,
 * Text Domain:       cf-wp-admin
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

/**
 * Register and enqueue a custom stylesheet in the WordPress admin.
 */
function cf_wp_admin_init() {
    wp_register_style( 'custom_wp_admin_css', plugin_dir_url( __FILE__ ).'main.css', false, '1.0.0' );
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'cf_wp_admin_init' );